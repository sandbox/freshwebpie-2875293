(function($, Drupal) {

  Drupal.behaviors.ebCrop = {
    attach: function (context) {
      $('.ebcrop-box').cropBox({
        reset: Drupal.imageWidgetCrop.reset,
        update: updateCropValues
      });
    }
  };

  /**
   * Callback taken and modified from the cropend which never gets
   * called in the image widget crop behaviours for our entity browser integeration.
   *
   * @param element
   * @param cropperValuesSelector
   */
  function updateCropValues(element, cropperValuesSelector) {
    var $values = element.siblings(cropperValuesSelector);
    var data = element.cropper('getData');
    // Calculate delta between original and thumbnail images.
    var delta = element.data('original-height') / element.prop('naturalHeight');
    /*
     * All data returned by cropper plugin multiple with delta in order to get
     * proper crop sizes for original image.
     */
    $values.find('.crop-x').val(Math.round(data.x * delta));
    $values.find('.crop-y').val(Math.round(data.y * delta));
    $values.find('.crop-width').val(Math.round(data.width * delta));
    $values.find('.crop-height').val(Math.round(data.height * delta));
    $values.find('.crop-applied').val(1);
    Drupal.imageWidgetCrop.updateCropSummaries(element);
  }

  // Provides the jQuery plugin for the crop toggle.
  $.fn.cropBox = function (options) {

    var settings = $.extend({
      triggerClass: 'trigger',
      contentClass: 'content',
      contentCloseClass: 'close',
      cropElementSelector: '.crop-preview-wrapper__preview-image',
      reset: function() {},
      // Callback to be used for updating values when leaving the crop box view
      // in case the underlying crop plugin handlers might not work quite the way you want.
      update: function() {}
    }, options);

    return this.each(function () {
      var parent = $(this);
      // Ensure that our crop view is setup by default as because our crop widget implementation
      // does not use the vertical tabs and other elements expected by the the ImageWidgetCrop default
      // initialiser.
      var cropElem = parent.find(settings.cropElementSelector);
      settings.reset(cropElem);
      // First bind to click event to show content when trigger is clicked.
      $(this).find('.' + settings.triggerClass).unbind('click').bind('click', function (evt) {
        var contentContainer = parent.find('.' + settings.contentClass);
        contentContainer.show();
      });
      // Now bind to close click events to hide the content section.
      $(this).find('.' + settings.contentCloseClass).unbind('click').bind('click', function (evt) {
        parent.find('.' + settings.contentClass).hide();
        // Let's update the crop values accordingly.
        settings.update(cropElem, '.crop-preview-wrapper__value');
      });
    });
  };

})(jQuery, Drupal);
