<?php

namespace Drupal\eb_crop\Plugin\EntityBrowser\SelectionDisplay;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eb_crop\EBCropManager;
use Drupal\entity_browser\SelectionDisplayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * Show current selection and delivers selected entities.
 *
 * @EntityBrowserSelectionDisplay(
 *   id = "eb_selection_display",
 *   label = @Translation("EB selection display"),
 *   description = @Translation("Hopefully allow for selected images to be cropped."),
 *   acceptPreselection = TRUE
 * )
 */
class EBSelectionDisplay extends SelectionDisplayBase {

  /**
   * Provides the manager to handle persisting crops
   * generated in the selection display view.
   *
   * @var \Drupal\eb_crop\EBCropManager
   */
  protected $cropManager;

  /**
   * Provides
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher,
                              EntityTypeManagerInterface $entity_type_manager, EBCropManager $crop_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager);
    $this->cropManager = $crop_manager;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('eb_crop.manager')
    );
  }

  /**
   * Returns selection display form.
   *
   * @param array $original_form
   *   Entire form built up to this point. Form elements for selection display
   *   should generally not be added directly to it but returned from function
   *   as a separated unit.dr
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   Form structure.
   */
  public function getForm(array &$original_form, FormStateInterface $form_state) {

    $selected_entities = $form_state->get(['entity_browser', 'selected_entities']);

    $form = [];

    foreach ($selected_entities as $file) {
      $form['image_crop'] = [
        '#type' => 'eb_image_crop',
        '#file' => $file,
        '#crop_type_list' => ['test_crop'],
        '#crop_preview_image_style' => 'crop_thumbnail',
        '#crop_viewport_image_style' => 'crop_preview',
        '#show_default_crop' => TRUE,
        '#show_crop_area' => TRUE,
        '#warn_multiple_usages' => TRUE,
      ];
    }

    $form['use_selected'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Use selection'),
      '#name' => 'use_selected',
    );
    
    return $form;
  }

  public function submit(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] == 'use_selected') {
      $crop_type = $this->entityTypeManager->getStorage('crop_type')->load('test_crop');
      $field_value = $form_state->getValue('image_crop');
      $properties = $field_value['crop_wrapper']['test_crop']['crop_container']['values'];
      // If properties are loaded then let's apply the crop to create a new image style version
      // of the file with the crop applied.
      if ($properties) {
        $this->cropManager->applyCrop($properties, $field_value, $crop_type);
      }
      $this->selectionDone($form_state);
    }
  }
}
