<?php

namespace Drupal\eb_crop\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\crop\Entity\Crop;
use Drupal\crop\Plugin\ImageEffect\CropEffect;

/**
 * Crops an image resource with extended support for parent entity
 * based crop instances.
 *
 * @ImageEffect(
 *   id = "eb_crop_crop",
 *   label = @Translation("Manual per entity crop"),
 *   description = @Translation("Applies manually provided crop to images utilising per parent entity functionality.")
 * )
 */
class EBCropEffect extends CropEffect {

  /**
   * Gets crop entity for the image.
   *
   * @param ImageInterface $image
   *   Image object.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\crop\CropInterface|FALSE
   *   Crop entity or FALSE if crop doesn't exist.
   */
  protected function getCrop(ImageInterface $image) {
    if (!isset($this->crop)) {
      $this->crop = FALSE;
      if ($crop = Crop::findCrop($image->getSource(), $this->configuration['crop_type'])) {
        $this->crop = $crop;
      }
    }

    return $this->crop;
  }
}
