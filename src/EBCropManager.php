<?php

namespace Drupal\eb_crop;

use Drupal\crop\Entity\CropType;
use Drupal\image_widget_crop\ImageWidgetCropManager;

class EBCropManager extends ImageWidgetCropManager {

  /**
   * {@inheritdoc}
   */
  public function saveCrop(array $crop_properties, $field_value, array $image_styles, CropType $crop_type, $notify = TRUE) {
    $values = [
      'type' => $crop_type->id(),
      'entity_id' => $field_value['file-id'],
      'entity_type' => 'file',
      'uri' => $field_value['file-uri'],
      'x' => $crop_properties['x'],
      'y' => $crop_properties['y'],
      'width' => $crop_properties['width'],
      'height' => $crop_properties['height'],
    ];

    // Save crop with previous values.
    /** @var \Drupal\crop\CropInterface $crop */
    $crop = $this->cropStorage->create($values);
    // If there are existing crops with the provided file uri and crop type
    // combination make sure we remove them.
    $existing_crops = $this->cropStorage->loadByProperties(['type' => $crop_type->id(), 'uri' => $field_value['file-uri']]);
    foreach ($existing_crops as $existing_crop) {
      $existing_crop->delete();
    }
    $crop->save();
    // Also we need to ensure we flush the image styles provided for this particular file uri.
    foreach ($image_styles as $style) {
      $style->flush($field_value['file-uri']);
    }

    if ($notify) {
      drupal_set_message(t('The crop "@cropType" was successfully added for image "@filename".', ['@cropType' => $crop_type->label(), '@filename' => $this->fileStorage->load($field_value['file-id'])->getFilename()]));
    }
  }
}
