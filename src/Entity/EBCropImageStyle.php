<?php

namespace Drupal\eb_crop\Entity;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\image\Entity\ImageStyle;

class EBCropImageStyle extends ImageStyle {

  /**
   * {@inheritdoc}
   */
  public function buildUri($uri) {
    $source_scheme = $scheme = $this->fileUriScheme($uri);
    $default_scheme = $this->fileDefaultScheme();

    if ($source_scheme) {
      $path = $this->fileUriTarget($uri);
      // In the case the image style utilises the crop effect
      // provided by the EBCropEffect plugin then we need to prepend
      // a suffix to allow for multiple instances.
      $effects = $this->getEffects();
      if ($effects->has('eb_crop_crop')) {
        // Now load the correct crop for the parent entity.

      }

      // The scheme of derivative image files only needs to be computed for
      // source files not stored in the default scheme.
      if ($source_scheme != $default_scheme) {
        $class = $this->getStreamWrapperManager()->getClass($source_scheme);
        $is_writable = $class::getType() & StreamWrapperInterface::WRITE;

        // Compute the derivative URI scheme. Derivatives created from writable
        // source stream wrappers will inherit the scheme. Derivatives created
        // from read-only stream wrappers will fall-back to the default scheme.
        $scheme = $is_writable ? $source_scheme : $default_scheme;
      }
    }
    else {
      $path = $uri;
      $source_scheme = $scheme = $default_scheme;
    }
    return "$scheme://styles/{$this->id()}/$source_scheme/{$this->addExtension($path)}";
  }
}
